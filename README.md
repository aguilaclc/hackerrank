Hackerrank solutions
======

**Solutions to ProjectEuler contests and more challenges**

### Listed solutions

| Problems | Description |
| :---------------------- | :------------------------------------------------------------------------: |
| [Alternating Characters]  | Find the minimum number of deletions needed to get an alternating string. |
| [Amicable Numbers] | Evaluate the sum of all the amicable numbers under N. |
| [Camelcase] | Get total amount of words in a string. |
| [Circular Array Rotation] | Get amount of times a rotation is made in a given array. |
| [Circular Primes] | Find the sum of all circular primes below N. |
| [Coded Triangle Numbers] | Get the index of any given triangular number. |
| [Collatz Sequence] | Get the largest Collatz sequence that starts with a number not greater than N. |
| [Counting Sundays] | How many Sundays fell on the first of the month between two dates? |
| [Digit nth Powers] | Find the sum of all the numbers that can be written as the sum of Nth powers of their digits. |
| [Double Base Palindromes] | Get the sum of k-palindromic and 10-palindromic numbers lower than N. |
| [Even Fibonacci Numbers] | Get the sum of any even Fibonacci number lower than N. |
| [Factorial Digits] | Get the sum of the digits of the factorial of a given number. |
| [Fibonacci Length] | Get the first term in the Fibonacci sequence to contain N digits. |
| [Get the Prime] | What is the Nth prime number? |
| [Large Sum] | Work out the first 10 digits of the sum of N 50-digit numbers. |
| [Largest Product] | Find the greatest product of K consecutive digits in the N digit number. |
| [Lattice Paths] | How many Lattice paths are in a N x M grid? |
| [Lexicographic Permutations] | What is the Nth lexicographic permutation of _abcdefghijklm_? |
| [Lonely Integer] | Find the element that is unique in a given array. |
| [Maximum Path Sum] | Find the maximum total from top to bottom of the triangle given in input. |
| [Maximum Perimeter Triangle] | Get the longest possible perimeter for a triangle given an array of numbers. |
| [Names Scores] | You are given Q queries, each query is a name, you have to print the score. |
| [New Year Chaos] | Find out if its possible for a queue to be modified in a certain way. |
| [Non Abundant Sums] | Find out if a number can be expressed as the sum of two abundant numbers. |
| [Non Divisible Subset] | Get the biggest subset where the sum of every 2 numbers is not divisible by K. |
| [Number Spiral Diagonals] | Get the sum of the numbers on the diagonals of a square N x N. |
| [Numbers to Words] | Given a number, write it in words. |
| [Power Digit Sum] | Get the sum of the digits of any power of 2. |
| [Powerful Digit Counts] | Print all the N-digit numbers that are also an Nth power. |
| [Palindrome Product] | Find the largest product of two 3-digit numbers that is also a palindrome. |
| [Pandigital Primes] | What is the largest n-digit pandigital prime below N? |
| [Pandigital Products] | Find all # where a*b=c identity can be written as a 1 through pandigital. |
| [Prime Factors] | Get the largest prime factor of a given number. |
| [Pythagorean Triplets] | Get the pythagorean triplet, using the sum of its elements. |
| [Quadratic Primes] | Find the coefficients for the quadratic expression that makes the longest sequence of primes. |
| [Reciprocal Primes] | Find the value of the longest recurring cycle for 1/N |
| [Richie Rich] | Transform a given number into a palindrome, if its possible. |
| [Self Powers] | Find the last ten digits of 1^1 + 2^2 + ... + n^n |
| [Smallest Multiple] | Get the smallest positive number divisible by every number from 1 to N. |
| [Substring Divisibility] | Find the sum of all 0 to N pandigital numbers with the substring property. |
| [Sum of Primes] | Get the sum of all prime numbers lower than a given integer. |
| [Square Difference] | Find the difference between the sum of squares and the square of the sum. |
| [The Grid] | Get the greatest product of four adjacent numbers in a 20x20 grid. |
| [Three And Five] | Find the sum of all the multiples of 3 and 5 lower than N. |
| [Triangular Numbers] | Find the first triangular number to have more than N divisors. |
| [Truncatable Primes] | Find the sum of two-way truncatable primes below N. |

[Alternating Characters]: https://www.hackerrank.com/challenges/alternating-characters
[Amicable Numbers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler021
[Camelcase]: https://www.hackerrank.com/challenges/camelcase
[Circular Array Rotation]: https://www.hackerrank.com/challenges/circular-array-rotation
[Circular Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler035
[Coded Triangle Numbers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler042
[Collatz Sequence]: https://www.hackerrank.com/contests/projecteuler/challenges/euler014
[Counting Sundays]: https://www.hackerrank.com/contests/projecteuler/challenges/euler019
[Digit nth Powers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler030
[Double Base Palindromes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler036
[Even Fibonacci Numbers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler002
[Factorial Digits]: https://www.hackerrank.com/contests/projecteuler/challenges/euler020
[Fibonacci Length]: https://www.hackerrank.com/contests/projecteuler/challenges/euler025
[Get the Prime]: https://www.hackerrank.com/contests/projecteuler/challenges/euler007
[Large Sum]: https://www.hackerrank.com/contests/projecteuler/challenges/euler013
[Largest Product]: https://www.hackerrank.com/contests/projecteuler/challenges/euler008
[Lattice Paths]: https://www.hackerrank.com/contests/projecteuler/challenges/euler015
[Lexicographic Permutations]: https://www.hackerrank.com/contests/projecteuler/challenges/euler024
[Lonely Integer]: https://www.hackerrank.com/challenges/lonely-integer
[Maximum Path Sum]: https://www.hackerrank.com/contests/projecteuler/challenges/euler018/
[Maximum Perimeter Triangle]: https://www.hackerrank.com/challenges/maximum-perimeter-triangle
[Names Scores]: https://www.hackerrank.com/contests/projecteuler/challenges/euler022
[New Year Chaos]: https://www.hackerrank.com/challenges/new-year-chaos
[Non Abundant Sums]: https://www.hackerrank.com/contests/projecteuler/challenges/euler023
[Non Divisible Subset]: https://www.hackerrank.com/challenges/non-divisible-subset
[Numbers to Words]: https://www.hackerrank.com/contests/projecteuler/challenges/euler017
[Number Spiral Diagonals]: https://www.hackerrank.com/contests/projecteuler/challenges/euler028
[Pandigital Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler041
[Pandigital Products]: https://www.hackerrank.com/contests/projecteuler/challenges/euler032
[Power Digit Sum]: https://www.hackerrank.com/contests/projecteuler/challenges/euler016
[Powerful Digit Counts]: https://www.hackerrank.com/contests/projecteuler/challenges/euler063
[Palindrome Product]: https://www.hackerrank.com/contests/projecteuler/challenges/euler004
[Prime Factors]: https://www.hackerrank.com/contests/projecteuler/challenges/euler003
[Pythagorean Triplets]: https://www.hackerrank.com/contests/projecteuler/challenges/euler009
[Quadratic Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler027
[Reciprocal Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler026
[Richie Rich]: https://www.hackerrank.com/challenges/richie-rich
[Smallest Multiple]: https://www.hackerrank.com/contests/projecteuler/challenges/euler005
[Self Powers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler048
[Substring Divisibility]: https://www.hackerrank.com/contests/projecteuler/challenges/euler043
[Sum of Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler010
[Square Difference]: https://www.hackerrank.com/contests/projecteuler/challenges/euler006
[The Grid]: https://www.hackerrank.com/contests/projecteuler/challenges/euler011
[Three And Five]: https://www.hackerrank.com/contests/projecteuler/challenges/euler001
[Triangular Numbers]: https://www.hackerrank.com/contests/projecteuler/challenges/euler012
[Truncatable Primes]: https://www.hackerrank.com/contests/projecteuler/challenges/euler037
