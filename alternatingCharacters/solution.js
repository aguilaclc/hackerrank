function filterWord(string) {
  let segments = string.match(/(A+|B+)/g);
  return string.length - segments.length;
}

function processData(input) {
  let lines = input.split("\n");
  let count = +lines.shift();
  for (let i = 0; i < count; i++) {
    console.log(filterWord( lines[i] ));
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = filterWord;