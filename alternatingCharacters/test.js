var assert = require("assert");
var filterWord = require("./solution");

describe("filterWord()", function () {
  it("should return n-1 if only one repeated char", function () {
    assert.equal(filterWord("AAAA"), 3);
    assert.equal(filterWord("BBBBB"), 4);
  });

  it("should return 0 if there's no changes", function () {
    assert.equal(filterWord("ABABABAB"), 0);
    assert.equal(filterWord("BABABA"), 0);
  });

  it("should perform changes and return deletions", function () {
    assert.equal(filterWord("AAABAAABBBA"), 6);
    assert.equal(filterWord("AAABBB"), 4);
  });
});