# Method used to generate the array of results. Answer is hard-coded

# require 'prime'

# def get_sum_of_divisors (num)
#   return 1 if num.prime?
#   fact, divs = [], [1]  
#   num.prime_division.each do |x|
#     x[1].times { fact << x[0] }
#   end    
#   (1..fact.size-1).each do |x|
#     divs.concat(fact.combination(x).to_a.map {|y| y.reduce(:*)})
#   end  
#   return divs.uniq.sort.reduce(:+)
# end

# def generate_amicables
#   (1..100000).each do |x|
#     next if x.prime?
#     y = get_sum_of_divisors(x)
#     res << x if x != y && x == get_sum_of_divisors(y)
#   end
#   return res
# end

# res = generate_amicables
res = [220, 284, 1184, 1210, 2620, 2924, 5020, 5564, 6232, 6368, 10744, 10856, 12285, 14595, 17296, 18416, 63020, 66928, 66992, 67095, 69615, 71145, 76084, 79750, 87633, 88730]

gets.to_i.times do
  n = gets.to_i
  puts res.take_while{|a| a < n }.reduce(0, :+)
end