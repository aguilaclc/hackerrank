process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function countWords(string) {
  return string ? 1 + (string.match(/[A-Z]/g) || []).length : 0;
};

function main() {
  var s = readLine();
  console.log(countWords(s));
}

module.exports = countWords;