var assert = require("assert");
var countWords = require("./solution");

describe("countWords()", function () {
  it("should count words in camel case", function () {
    assert.equal(countWords("anitaLavaLaTina"), 4);
    assert.equal(countWords("holaBuenosDias"), 3);
    assert.equal(countWords("helloWorld"), 2);
  });

  it("should return zero on empty string", function () {
    assert.equal(countWords(""), 0);
  });

  it("should return one when there's no uppercase char", function () {
    assert.equal(countWords("countingincamelcase"), 1);
  });
});