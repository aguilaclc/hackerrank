function processData(input) {
  let lines = input.split("\n");
  let [n,k,q] = lines.shift().split(" ").map(x => +x);
  let array = lines.shift().split(" ");

  array = rotateArray(array, k);

  for (let b = 0; b < q; b++) {
    console.log(array[lines.shift()]);
  }
}

function rotateArray(array, k) {
  k = k % array.length;
  return array.splice(-k).concat(array);
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = rotateArray;