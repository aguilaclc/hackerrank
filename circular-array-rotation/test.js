var assert = require("assert");
var rotateArray = require("./solution");

describe("rotateArray()", function () {
  it("should rotate clockwise", function () {
    assert.deepEqual(rotateArray([1, 2, 3, 4], 2), [3, 4, 1, 2]);
    assert.deepEqual(rotateArray([7, 8, 12, 32], 3), [8, 12, 32, 7]);
  });

  it("should rotate counterclockwise, when given a negative arg", function () {
    assert.deepEqual(rotateArray([1, 2, 3, 4], -1), [2, 3, 4, 1]);
  });

  it("should do nothing with an empty array", function () {
    assert.deepEqual(rotateArray([], 2), []);
  });

  it("should not rotate array when arg is zero", function () {
    assert.deepEqual(rotateArray(["a", "b", "c"], 0), ["a", "b", "c"]);
  });
});