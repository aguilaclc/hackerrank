def collatz (num)
  size = 0
  orig = num
  while true do
    if num < orig then
      return size + @results[num-1]
    elsif num % 2 == 0 then
      num = num/2
      size += 1
    else
      num = (3 * num + 1) / 2
      size += 2
    end
  end
end

nums = gets.to_i.times.map { gets.to_i }

@results = [1]
@theOutput = [1]
maxKey = 0

(1..nums.max-1).each do |x|
  if !@results[x] then
    @results[x] = collatz(x+1)
  end
  res = @results[x]
  if res >= @results[maxKey] then
    maxKey = x
  end
  @theOutput[x+1] = maxKey+1
end

nums.each do |x|
  puts @theOutput[x]
end