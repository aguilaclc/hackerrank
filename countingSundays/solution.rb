require 'date'

gets.to_i.times do
  startYear, startMonth, startDay = gets.chomp.split(' ').map{|x| x.to_i}
  endYear, endMonth, endDay = gets.chomp.split(' ').map{|x| x.to_i}
  count = 0
  
  if startDay != 1 then
    startDay = 1  
    if startMonth == 12 then
      startMonth = 1
      startYear += 1
    else 
      startMonth += 1
    end
  end
  
  while endYear > startYear do
    while startMonth <= 12 do
      if Date.new(startYear, startMonth, 1).wday == 0 then
        count += 1
      end
      startMonth += 1
    end
    startYear += 1
    startMonth = 1
  end
  
  while endMonth >= startMonth do
    if Date.new(startYear, startMonth, 1).wday == 0 then
      count += 1
    end
    startMonth += 1
  end
    
  puts count
end