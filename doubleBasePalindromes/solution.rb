n, k = gets.split(' ').map{|x| x.to_i}
c = 0

(1..n-1).each do |x|
  c += x if x.to_s == x.to_s.reverse && x.to_s(k) == x.to_s(k).reverse 
end

puts c