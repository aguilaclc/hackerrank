function getFibonacci(top) {
  let result = 0;
  let a,b;
  let c = 0;
  while (c < top) {
    a = a ? b + c : 1;
    b = b ? c + a : 2;
    c = a + b;
    result += b;
  }
  if (b > top) result -= b;
  return result;
}

module.exports = getFibonacci;