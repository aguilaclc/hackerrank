var assert = require("assert");
var getFibonacci = require("./solution");

describe("getFibonacci()", function () {
  it("should return the sum of even fibonacci numbers lower than N", function () {
    assert.equal(getFibonacci(10), 10);
    assert.equal(getFibonacci(100), 44);
  });
});