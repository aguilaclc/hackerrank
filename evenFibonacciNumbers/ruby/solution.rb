def fibonacci (top)
  a = 1
  b = 2
  c = 0

  result = b

  while b < top do
    c = a + b
    a = b + c
    b = a + c
    result += b
  end

  if b > top then result -= b end

  result
end

gets.to_i.times do
  puts fibonacci(gets.to_i)
end