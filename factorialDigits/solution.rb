def factorial(n)
  return if n < 0
  return 1 if n == 0
  n * factorial(n - 1)
end

(gets.to_i).times do
  puts factorial(gets.to_i).to_s.split('').map{|x| x.to_i}.reduce(:+)
end