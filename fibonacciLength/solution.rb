wGOLDEN_RATIO = (1 + Math.sqrt(5)) / 2

def len_fib(n)
  return n * Math.log(GOLDEN_RATIO, 10) - Math.log(5, 10) / 2 + 1
end

values = gets.to_i.times.map { gets.to_i }
count = 1

n_digits_fibos = (1..(values.max)).map { |n|
  count += 1 while len_fib(count) < n
  count
}

values.each { |n|
  puts n_digits_fibos[n-1]
}