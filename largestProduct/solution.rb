gets.to_i.times do
  capture = gets.chomp().split(' ')
  n = capture[0].to_i
  k = capture[1].to_i
  num = gets.chomp()
  result = 0

  (0..n-k).each do |x|
    value = num[x..x+k-1].split('').map{|x| x.to_i}.reduce(:*)
    if value > result then result = value end
  end

  puts result
end