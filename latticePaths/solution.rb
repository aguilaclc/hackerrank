$mod = 10**9 + 7

gets.to_i.times do
  res = gets.chomp.split(" ").map{|x| x.to_i}
  num = Array.new(res[0]+res[1]).combination(res[1]).size
  puts num % $mod
end