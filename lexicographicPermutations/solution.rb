LETTERS = "abcdefghijklm".chars

def fac (n)
  n < 2 ? 1 : n * fac(n-1)
end

gets.to_i.times do
  str = LETTERS.clone
  n = gets.to_i - 1
  l, c = 12, nil
  x = ""
  
  begin
    c, n = n.divmod fac(l)
    x << str.delete_at(c)
    l -= 1
  end while n > 0
  
  puts "#{x}#{str.join}"
end