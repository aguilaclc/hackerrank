function findUnique(array) {
  let object = {};
  for (let x of array) {
    object[x] = object[x] ? false : true;
  }

  for (let num in object) {
    if (object[num]) return num;
  }
}

function processData(input) {
  let lines = input.split("\n");
  let n = lines.shift();
  let array = lines.shift().split(" ");
  console.log(findUnique(array));
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = findUnique;