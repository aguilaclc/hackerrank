var assert = require("assert");
var findUnique = require("./solution");

describe("findUnique()", function () {
  it("should find unique value, case 1", function () {
    assert.equal(findUnique([1]), 1);
  });

  it("should find unique value, case 2", function () {
    assert.equal(findUnique([1,1,2]), 2);
  });

  it("should find unique value, case 3", function () {
    assert.equal(findUnique([0,0,1,2,1]), 2);
  });
});