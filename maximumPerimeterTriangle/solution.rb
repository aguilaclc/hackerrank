n = gets.to_i
v = gets.chomp.split(' ').map{|x| x.to_i}.sort
r = v.combination(3).select{|x| x[0] + x[1] > x[2]}.uniq
puts r.size > 0 ? r.last.join(' ') : -1