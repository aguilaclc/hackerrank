process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function bribeQueue(array, length) {
  let count = 0;
  let acc = 0;

  for (let i = 0; i < length; i++) {
    let value = +array[i];
    if (value > i + 3) return "Too chaotic";
    if (value >= acc + 3) {
      count += 2;
    } else if (value == acc + 2) {
      count ++;
      if (value <= i + 1) {
        acc += i - value + 2;
      }
      acc ++;
    } else if (value < acc + 2) {
      acc = i + 1;
    }
  }
  return count;
}

function main() {
  var T = +readLine();
  for(var x = 0; x < T; x++){
    var n = +readLine();
    let q = readLine().split(' ');
    console.log(bribeQueue(q, n));
  }
}

module.exports = bribeQueue;