var assert = require("assert");
var bribeQueue = require("./solution");

describe("bribeQueue()", function () {
  it("should return the amount of bribes", function () {
    assert.equal(bribeQueue([2,1,5,3,4], 5), 3);
    assert.equal(bribeQueue([3,4,5,2,1], 5), 7);
  });

  it("should tell us that it can't be done", function () {
    assert.equal(bribeQueue([2,5,1,3,4], 5), "Too chaotic");
  });
});