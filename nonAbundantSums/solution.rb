require 'prime'

def is_abundant (num)
  return false if num.prime?
  fact, divs = [], [1]
  
  num.prime_division.each do |x|
    x[1].times { fact << x[0] }
  end  
  
  (1..fact.size-1).each do |x|
    divs.concat(fact.combination(x).to_a.map {|y| y.reduce(:*)})
  end
  
  return divs.uniq.sort.reduce(:+) > num
end

def is_sum_of_abundants (num)
  return true if $abundants.include?(num/2.0)
  index = 0
  while (num - $abundants[index] > 12) do
    return true if $abundants.include?(num - $abundants[index])
    index += 1
  end
  return false
end

$abundants = []
vals = gets.to_i.times.map{gets.to_i}
max = vals.max > 28123 ? 28123 : vals.max
(12..max).each {|a| $abundants << a if is_abundant(a)}

vals.each do |x|
  if x > 28123 || is_sum_of_abundants(x) then
    puts "YES"
  else
    puts "NO"
  end
end