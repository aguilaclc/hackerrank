var getCount = require("./solution");

function processData(input) {
  let lines = input.split("\n");
  let [n, k] = lines.shift().split(" ").map(x => +x);
  let nums = lines.shift().split(" ");
  console.log(getCount(nums, k));
} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
  _input += input;
});

process.stdin.on("end", function () {
 processData(_input);
});