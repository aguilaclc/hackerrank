function getRemainders(array, k) {
  let remainders = {};
  array.forEach(function (x) {
    let rem = +x % k;
    remainders[rem] = 1 + (remainders[rem] || 0);
  });
  return remainders;
}

function getCount(array, k) {
  let remainders = getRemainders(array, k);
  let result = 0;
  for (let rem in remainders) {
    if (rem == 0 || rem == k-rem) { result += 2; } else {
      result += remainders[k-rem] ? Math.max(remainders[rem], remainders[k-rem]) : 2*remainders[rem];
    }
  }
  return result/2;
}

module.exports = getCount;