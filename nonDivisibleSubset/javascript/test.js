var assert = require("assert");
var getCount = require("./solution");

describe("getCount()", function () {
  it("should return the largest possible length, case 1", function () {
    assert.equal(getCount([1,7,2,4], 3), 3);
  });

  it("should return the largest possible length, case 2", function () {
    assert.equal(getCount([3,2,12,19,16,21,7], 8), 6);
  });

  it("should return the largest possible length, case 3", function () {
    assert.equal(getCount([1,2,32,13,15,21,7], 2), 2);
  });
});