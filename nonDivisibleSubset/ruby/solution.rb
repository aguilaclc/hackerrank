rems = {}
result = 0
k = gets.chomp.split(' ')[1].to_i
array = gets.chomp.split(' ').map{|x| x.to_i}

array.each do |x|
  rems[x % k] = 1 + (rems[x % k] || 0)
end

rems.each do |key, val|
  if (key == 0 || key == k - key) then
    result += 2
  else
    result += rems[k-key] ? [val, rems[k-key]].max : 2 * val
  end
end

puts result / 2