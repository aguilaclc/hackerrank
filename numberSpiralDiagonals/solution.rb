mod = 10**9 + 7

gets.to_i.times do
  n = gets.to_i
  puts (2*n*(n**2 + 2) / 3 + (n**2 - 1) / 2 - 1) % mod
end