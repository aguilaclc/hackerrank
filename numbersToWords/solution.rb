$digits = {"1"=>"One", "2"=>"Two", "3"=>"Three", "4"=>"Four", "5"=>"Five", "6"=>"Six", "7"=>"Seven", "8"=>"Eight", "9"=>"Nine"}

$special = {"10"=>"Ten", "11"=>"Eleven", "12"=>"Twelve", "13"=>"Thirteen", "14"=>"Fourteen", "15"=>"Fifteen", "16"=>"Sixteen", "17"=>"Seventeen", "18"=>"Eighteen", "19"=>"Nineteen"}

$tens = {"2"=>"Twenty", "3"=>"Thirty", "4"=>"Forty", "5"=>"Fifty", "6"=>"Sixty", "7"=>"Seventy", "8"=>"Eighty", "9"=>"Ninety"}

def hundreds (num)
  result = []
  if num[0] == "0" then num.slice!(0) end
  val = num.to_i

  if val < 10 then
    result << $digits[val.to_s]
  elsif val < 20 then
    result << $special[val.to_s]
  elsif val < 100 then
    result << $tens[num.slice!(0)]
    result << $digits[num] if num.to_i > 0
  elsif val < 1000 then
    result << "#{$digits[num.slice!(0)]} Hundred"
    result.concat(hundreds(num))
  end

  return result
end

def thousands (num)
  result = []
  rest = num.slice!(-3..-1)

  if num.to_i > 0 then
    result.concat(hundreds(num))
    result << "Thousand"
  end
  result.concat(hundreds(rest))

  return result
end

def millions (num)
  result = []
  rest = num.slice!(-6..-1)

  if num.to_i > 0 then
    result.concat(hundreds(num))
    result << "Million"
  end
  result.concat(thousands(rest))

  return result
end

def billions (num)
  result = []
  rest = num.slice!(-9..-1)

  result.concat(hundreds(num))
  result << "Billion"
  result.concat(millions(rest))

  return result
end

gets.to_i.times do
  num = gets.chomp
  comp = num.to_i
  result = []

  if comp < 1000 then
    result.concat(hundreds(num))
  elsif comp < 1000000 then
    result.concat(thousands(num))
  elsif comp < 1000000000
    result.concat(millions(num))
  else
    result.concat(billions(num))
  end

  puts result.reject{|x| x.nil? || x.empty?}.join(" ")
end