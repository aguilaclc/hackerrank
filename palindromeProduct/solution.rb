def getAllNumbers
  array = []

  (100..999).each do |x|
    (100..999).each do |y|
      val = x * y
      if val.to_s == val.to_s.reverse then array << val end
    end
  end
  return array.uniq.sort
end

$palindromes = getAllNumbers

def getMax(a)
  (0..$palindromes.size).each do |x|
    if $palindromes[x] >= a then
      return $palindromes[x-1]
    end
  end
end

gets.to_i.times do
  puts getMax(gets.to_i)
end