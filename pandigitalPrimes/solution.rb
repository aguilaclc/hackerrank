require 'prime'

n = [1423, 2143, 2341, 4231]
[1,2,3,4,5,6,7].permutation().each do |x|
  n << x.join.to_i if x.join.to_i.prime?
end

gets.to_i.times do
  b = gets.to_i
  puts n.take_while{|a| a <= b}.last || -1
end