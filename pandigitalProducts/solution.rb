def filter(x, n)
  if n == 9
    return 1000*x[5]+100*x[6]+10*x[7]+x[8] if (10*x[0]+x[1])*(100*x[2]+10*x[3]+x[4]) == 1000*x[5]+100*x[6]+10*x[7]+x[8]
    return 1000*x[5]+100*x[6]+10*x[7]+x[8] if x[0]*(1000*x[1]+100*x[2]+10*x[3]+x[4]) == 1000*x[5]+100*x[6]+10*x[7]+x[8]
  elsif n == 8
    return 1000*x[4]+100*x[5]+10*x[6]+x[7] if (10*x[0]+x[1])*(10*x[2]+x[3]) == 1000*x[4]+100*x[5]+10*x[6]+x[7]
    return 1000*x[4]+100*x[5]+10*x[6]+x[7] if x[0]*(100*x[1]+10*x[2]+x[3]) == 1000*x[4]+100*x[5]+10*x[6]+x[7]
  elsif n == 7
    return 100*x[4]+10*x[5]+x[6] if (10*x[0]+x[1])*(10*x[2]+x[3]) == 100*x[4]+10*x[5]+x[6]
    return 100*x[4]+10*x[5]+x[6] if x[0]*(100*x[1]+10*x[2]+x[3]) == 100*x[4]+10*x[5]+x[6]
  elsif n == 6
    return 100*x[3]+10*x[4]+x[5] if (10*x[1]+x[2])*x[0] == 100*x[3]+10*x[4]+x[5]
  elsif n == 5
    return 10*x[3]+x[4] if x[0]*(10*x[1]+x[2]) == 10*x[3]+x[4]
  elsif n == 4
    return 10*x[2]+x[3] if x[0]*x[1] == 10*x[2]+x[3]
  else
    return nil
  end
end

n = gets.to_i
a = []
(Array(1..n)).permutation().to_a.map{|x| a << filter(x,n) if filter(x, n)}
puts a.uniq.reduce(:+) || 0