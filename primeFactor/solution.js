function largestPrime(num) {
  let limit = Math.floor(Math.sqrt(num)) + 1;

  for (let i = 2; i < limit; i ++) {
    if (num % i == 0) return largestPrime(num / i);
  }

  return num;
}

function processData(input) {
  let lines = input.split("\n");
  let n = +lines.shift();

  for (let i = 0; i < n; i++) {
    console.log(largestPrime(+lines.shift()));
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = largestPrime;