function calculate(n) {
  if (n%2 != 0) return -1;
  for (let c = Math.floor(n / 3) + 1; c < n - 2; c ++) {
    for (let b = Math.floor((n - c) / 2) + 1; b < c; b ++) {
      let a = n - c - b;
      if (a > 0 && Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2) && a < b && b < c) {
        return a * b * c;
      }
    }
  }
  return -1;
}

function processData(input) {
  let lines = input.split("\n");
  let n = +lines.shift();

  for (let i = 0; i < n; i++) {
    console.log(calculate(+lines.shift()));
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = calculate;