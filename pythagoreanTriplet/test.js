var assert = require("assert");
var calculate = require("./solution");

describe("calculate()", function () {
  it("should calculate product, case 1", function () {
    assert.equal(calculate(4), -1);
  });

  it("should calculate product, case 2", function () {
    assert.equal(calculate(12), 60);
  });

  it("should calculate product, case 3", function () {
    assert.equal(calculate(30), 780);
  });
});