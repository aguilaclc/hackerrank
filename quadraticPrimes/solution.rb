require 'prime'

def getSequence(nums)
  a,b = nums
  count = 0
  loop do
    return count if !(count*(count + a) + b).prime?
    count += 1
  end
end

n = gets.to_i
a = n > 79 ? 79 : n
b = n > 1601 ? 1601 : n
pairs = (Array (-a..a)).product(Array (-b..b))
vals = pairs.map { |x| getSequence(x) }
    
puts pairs[vals.index(vals.max)].join(" ")