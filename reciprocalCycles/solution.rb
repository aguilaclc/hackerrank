require 'prime'

def get_length (x)
  c = 0
  o = 1
  begin
    o = 10*o % x
    c += 1
  end while o > 1 
  return 0 if o == 0
  return c if o == 1
end

n = gets.to_i.times.map{gets.to_i}
p = Prime.first(1229).take_while{|x| x < n.max}
r = p.map{|x| get_length x}
    
n.each do |x|
  ac = p.select{|a| a < x}.size - 1
  puts p[r.index(r[0..ac].max)]
end 