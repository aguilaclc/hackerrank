Sandy likes palindromes. A palindrome is a word, phrase, number, or other
sequence of characters which reads the same backward as it does forward. For
example, madam is a palindrome.

On her 7th birthday, Sandy's uncle, Richie Rich, offered her an n-digit check
which she refused because the number was not palindrome. Richie then challenged
Sandy to make the number palindromic by changing no more than k digits. Sandy
can only change 1 digit at a time, and cannot add digits to (or remove digits
from) the number.

Given k and an n-digit number, help Sandy determine the largest possible number
she can make by changing at most, k digits. Treat the integers as numeric
strings. Leading zeros are permitted and can't be ignored (So 0011 is not a
palindrome, 0110 is a valid palindrome). A digit can be modified more than once.

Input Format
The first line contains two space-separated integers, n and k, the number of
digits in the number and the maximum number of digits that can be altered.
The second line contains an n-digit string representing the number.

Output Format
Print a single line with the largest possible number that can be made, by
changing no more than k digits. If its not possible, print -1.
