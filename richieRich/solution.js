process.stdin.resume();
process.stdin.setEncoding('ascii');

let input_stdin = "";
let input_stdin_array = "";
let input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function toPalindrome(number, max, length) {
  let array = number.split("");
  let changes = {};
  let limit = Math.floor(length / 2);

  for (let i = 0; i < limit; i++) {
    if (array[i] == array[length-i-1]) {
      continue;
    } else if (max <= 0) {
      return "-1";
    }
    if (array[i] < array[length - i - 1]) {
      array[i] = array[length - i - 1];
      changes[i] = true;
    } else {
      array[length - i - 1] = array[i];
      changes[length - i - 1] = true;
    }
    max --;
  }

  for (let char = 0; char <= limit && max > 0; char++) {
    if (array[char] == "9") continue;
    let val = length-char-1;
    let needed = (changes[char] || changes[val] || char == val) ? 1 : 2;
    if (needed <= max) {
      array[char] = "9";
      array[val] = "9";
      max -= needed;
    }
  }
  return array.join("");
}

function main() {
  let n_temp = readLine().split(' ');
  let n = parseInt(n_temp[0]);
  let k = parseInt(n_temp[1]);
  let number = readLine();

  console.log(toPalindrome(number, k, n));
}

module.exports = toPalindrome;