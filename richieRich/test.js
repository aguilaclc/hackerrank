var assert = require("assert");
var toPalindrome = require("./solution");

describe("toPalindrome()", function () {
  it("should return a palindrome number", function () {
    assert.equal(toPalindrome("3943", 1, 4), "3993");
    assert.equal(toPalindrome("092282", 3, 6), "992299");
    assert.equal(toPalindrome("124421", 2, 6), "924429");
  });

  it("should return -1 when can't be palindrome", function () {
    assert.equal(toPalindrome("0011", 1, 4), "-1");
  });
});