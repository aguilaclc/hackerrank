m = 10**10
r = 0

def pow_exp (a,b,c)
  x = 1
  while b > 0 do
    x = x * a % c if b & 1 > 0
    b >>= 1
    a = a * a % c
  end
  x
end

(1..gets.to_i).each do |a|
  next if a % 10 == 0
  r = (r + pow_exp(a,a,m)) % m
end

puts r