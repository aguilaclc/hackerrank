a = Array(0..gets.to_i)
n = a.permutation(a.size)

if a.size >= 4
  n = n.select{|a| a[3] % 2 == 0}
end
if a.size >= 5
  n = n.select{|a| (a[2]+a[3]+a[4]) % 3 == 0}
end
if a.size >= 6
  n = n.select{|a| a[5] % 5 == 0}
end
if a.size >= 7
  n = n.select{|a| (100*a[4] + 10*a[5] + a[6]) % 7 == 0}
end
if a.size >= 8
  n = n.select{|a| (100*a[5] + 10*a[6] + a[7]) % 11 == 0}
end
if a.size >= 9
  n = n.select{|a| (100*a[6] + 10*a[7] + a[8]) % 13 == 0}
end
if a.size >= 10
  n = n.select{|a| (100*a[7] + 10*a[8] + a[9]) % 17 == 0}
end

puts n.map{|x| x.join.to_i}.reduce(:+)