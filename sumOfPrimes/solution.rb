require 'prime'

ns = gets.to_i.times.map { gets.to_i }
mx = ns.max
acc = 0
sums = {}

Prime.each do |p|
  break if p > mx
  sums[p] = acc += p
end

ns.each do |n|
  n -= 1 while !sums[n]
  puts sums[n]
end