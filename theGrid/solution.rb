grid = []
(1..20).each do
  grid << gets.chomp.split(' ').map{|x| x.to_i}
end

def maxHorizontal (array)
  result = 1
  (0..19).each do |x|
    (0..16).each do |y|
      val = array[x][y] * array[x][y+1] * array[x][y+2] * array[x][y+3]
      result = val if val > result
    end
  end
  return result
end

def maxVertical (array)
  result = 0;
  (0..19).each do |x|
    (0..16).each do |y|
      val = array[y][x] * array[y+1][x] * array[y+2][x] * array[y+3][x]
      result = val if val > result
    end
  end
  return result
end

def maxDiagLR (array)
  result = 0;
  (0..16).each do |x|
    (0..16).each do |y|
      val = array[x][y] * array[x+1][y+1] * array[x+2][y+2] * array[x+3][y+3]
      result = val if val > result
    end
  end
  return result
end

def maxDiagRL (array)
  result = 0;
  (3..19).each do |x|
    (0..16).each do |y|
      val = array[x-3][y+3] * array[x-2][y+2] * array[x-1][y+1] * array[x][y]
      result = val if val > result
    end
  end
  return result
end

puts [maxHorizontal(grid), maxVertical(grid), maxDiagLR(grid), maxDiagRL(grid)].max