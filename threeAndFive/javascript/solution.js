function getSum(num) {
  return num * (num+1) / 2;
}

function ThreeAndFive (num) {
  num --;

  let res3 = 3 * getSum(Math.floor(num / 3));
  let res5 = 5 * getSum(Math.floor(num / 5));
  let res15 = 15 * getSum(Math.floor(num / 15));

  return res3 + res5 - res15;
}

function processData(input) {
  let lines = input.split("\n");
  let n = +lines.shift();

  for (let i = 0; i < n; i ++) {
     console.log(+ThreeAndFive(+lines.shift()));
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = ThreeAndFive;