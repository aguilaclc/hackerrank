var assert = require("assert");
var threeAndFive = require("./solution");

describe("threeAndFive()", function () {
  it("should return the result of the assertion", function () {
    assert.equal(threeAndFive(10), 23);
    assert.equal(threeAndFive(16), 60);
    assert.equal(threeAndFive(100), 2318);
  });
});