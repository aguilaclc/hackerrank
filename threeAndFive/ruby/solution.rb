def getSum(num)
  return num*(num+1)/2
end

def calculate(num)
  num -= 1;
  res3 = 3 * getSum((num/3).floor)
  res5 = 5 * getSum((num/5).floor)
  res15 = 15 * getSum((num/15).floor)
  return res3 + res5 - res15;
end

i = 0
n = gets.chomp().to_i
while i < n do
  puts calculate(gets.chomp().to_i)
  i += 1
end