function getTau (num) {
  if (num == 1) return 1;
  let limit = Math.floor(num / 2) + 1;
  let count = 1;

  for (let i = 2; i < limit; i++) {
    if (num%i == 0) count ++;
  }

  return count + 1;
}

function getMax (amount) {
  let number = 1;
  let next = 2;

  while (true) {
    if (getTau(number) > amount) {
      return number;
    } else {
      number += next;
      next ++;
    }
  }
}

function processData(input) {
  let lines = input.split("\n");
  let n = +lines.shift();

  for (let i = 0; i < n; i++) {
    console.log(getMax(+lines.shift()));
  }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});

module.exports = getMax;