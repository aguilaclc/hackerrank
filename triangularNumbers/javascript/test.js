var assert = require("assert");
var getMax = require("./solution");

describe("getMax()", function () {
  it("should get first number with n divisors, case 1", function () {
    assert.equal(getMax(1), 3);
  });

  it("should get first number with n divisors, case 2", function () {
    assert.equal(getMax(2), 6);
  });

  it("should get first number with n divisors, case 3", function () {
    assert.equal(getMax(3), 6);
  });

  it("should get first number with n divisors, case 4", function () {
    assert.equal(getMax(4), 28);
  });
});