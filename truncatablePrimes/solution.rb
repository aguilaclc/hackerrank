require 'prime'

def truncatable (n)
  x = n.to_s.size - 1
  x.times do |m|
    a = n.to_s
    b = a.slice!(0..m)
    return false if !a.to_i.prime? || !b.to_i.prime?
  end
  return true
end

r = 0
n = gets.to_i

(23..n).each do |x|
  next if !x.prime?
  r += x if truncatable(x)
end

puts r